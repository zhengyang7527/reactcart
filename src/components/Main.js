import React, {Component} from 'react'
import {Route} from 'react-router-dom';
import {Redirect} from 'react-router'
import {createFilter} from 'react-search-input'
import json_products from '../products.json'
import ProductList from './ProductList'
import SideBar from './SideBar'
import Login from './Login'
import '../css/ProductList.css'
import Success from "./Success";
import axios from "axios/index";
import NavBar from "./NavBar";
import CompanyInfo from "./CompanyInfo";

const KEYS_TO_FILTERS = ['name', 'category'];
let active_products = [];

class Main extends Component {

    constructor(props) {
        super(props);
        this.state = {
            loggedIn: localStorage.getItem('token'),
            user_display_name: localStorage.getItem('user_display_name'),
            user_email: localStorage.getItem('user_email'),
            additionalEmail: '',
            username: '',
            password: '',
            searchTerm: '',
            currentCategory: '',
            product_response: undefined
        }
    }

    componentWillMount() {
        console.log(this.state.user_display_name);
        window.simpleCart({
            checkout: {
                type: "SendForm",
                // url: "http://173.255.241.13:5053/sendEmail",
                url: "/api/sendEmail",
                method: "POST",
                success: "/success",
                cancel: "cancel.html",
                extra_data: {
                    user_display_name: this.state.user_display_name,
                    toEmail: this.state.user_email + ',' + this.state.additionalEmail,
                }
            }
        });

        var self = this;
        axios.get('http://back.goldenharbor.ca/wp-json/wp/v2/products/', {
            params: {}
        })
            .then(function (response) {
                let fetched_products = response.data;
                let loop_id = 1000;
                fetched_products.map(fproduct => {
                    let product_id = fproduct.acf.id === undefined ? loop_id : fproduct.acf.id;
                    active_products.push(
                        {
                            "id": product_id,
                            "name": fproduct.acf.name,
                            "price": fproduct.acf.price,
                            // "image": fproduct.better_featured_image.media_details.sizes.medium.source_url,
                            "image": fproduct.better_featured_image.source_url,
                            "category": fproduct.acf.category,
                        }
                    );
                    loop_id += 1;
                })
                self.setState({
                    product_response: true,
                });
            })
            .catch(function (error) {
                console.log(error);
            });

    }

    render() {
        const filteredProducts = active_products.filter(createFilter(this.state.searchTerm, KEYS_TO_FILTERS));

        return (
            <div>
                <Route exact path="/" render={() => (
                    this.state.loggedIn ? (
                        <Redirect to="/products"/>
                    ) : (
                        <Redirect to="/login"/>
                    )
                )}/>

                <Route path='/products' render={() => (
                    this.state.loggedIn ? (
                        <div className="table">
                            <div className='col-xs-12'>
                                <NavBar searchUpdated={this.searchUpdated.bind(this)}
                                        chooseCategory={this.chooseCategory.bind(this)}
                                        handleLogout={this.handleLogout.bind(this)}/>
                            </div>
                            <div className='col-sm-8 col-xs-11'>
                                <ProductList filteredProducts={filteredProducts}/>
                            </div>
                            <div className="col-sm-4 col-xs-12">
                                <SideBar user_display_name={this.state.user_display_name}
                                         user_email={this.state.user_email}
                                         handleChange={this.handleChange.bind(this)}/>
                            </div>
                        </div>
                    ) : (
                        <Redirect to="/login"/>
                    )
                )}/>

                <Route path="/login" render={() => (
                    this.state.loggedIn ? (
                        <Redirect to='/products'/>
                    ) : (
                        <div>
                            <Login name='login' handleChange={this.handleChange.bind(this)}
                                   handleSubmit={this.handleSubmit.bind(this)}/>
                            <CompanyInfo/>
                        </div>

                    )
                )}/>

                <Route path="/success" render={(props) => (
                    <Success user_display_name={props.user_display_name}
                             toEmail={props.toEmail}/>
                )}/>
            </div>

        );
    }

    handleChange({target}) {
        console.log([target.name] + " = " + target.value);
        this.setState({
            [target.name]: target.value
        })
    }

    handleSubmit(event) {
        event.preventDefault();
        this.axiosToken(this.state.username, this.state.password);
    }

    axiosToken(username, password) {
        let self = this;
        axios.post('http://back.goldenharbor.ca/wp-json/jwt-auth/v1/token', {
            username: username,
            password: password
        })
            .then(response => {
                localStorage.setItem('token', 'Bearer ' + response.data.token);
                localStorage.setItem('user_display_name', response.data.user_display_name);
                localStorage.setItem('user_email', response.data.user_email);
                self.setState({
                    user_display_name: response.data.user_display_name,
                    user_email: response.data.user_email,
                    loggedIn: true
                })
            })
            .catch(function (error) {
                alert('Incorrect Username or/and Password, please try again');
                console.log(error);
            });
    }

    handleLogout() {
        localStorage.removeItem('token');
        this.setState({
            loggedIn: false
        })
    }

    searchUpdated(term) {
        this.setState({
            searchTerm: term
        })
    }

    chooseCategory(e) {
        this.setState({
            currentCategory: e.target.id,
            searchTerm: e.target.id
        })
    }
}

export default Main;
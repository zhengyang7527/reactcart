import React, {Component} from 'react';
import '../css/product.css'


class Product extends Component {

    render() {
        return (
            <div className="product">
                <div className="product-image">
                    <img alt="image" className="item_thumb thumb" src={this.props.product_info.image}/>
                </div>
                <h4>
                    <label>{this.props.product_info.id}.</label>
                    <label className="item_name">{this.props.product_info.name}</label>
                </h4>
                <span className="hidden col-xs-1 item_price">${this.props.product_info.price}  </span>
                <input type="hidden" className="item_shipping" value={this.props.product_info.shipping}/>
                <input type="text" className="col-xs-6 col-xs-offset-3 col-sm-4 col-sm-offset-4 item_quantity p0" defaultValue="100"/><br/><br/>
                <input type="button" className="btn-xs btn-success item_add" value="Add to Cart"/>
                <br/>

            </div>
        );
    }
}

export default Product;
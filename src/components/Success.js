import React, {Component} from 'react'
import {Link} from 'react-router-dom'
import '../css/success.css'

class Success extends Component {
    render() {
        return (
            <div className="row text-center">
                <br/><br/>
                <div className="col-sm-12 ">
                    <h2>Success</h2>
                    <h3>Dear {localStorage.getItem('user_display_name')}, </h3>
                    <br/>
                    <p className='col-xs-8 col-xs-offset-2'>Thank you for placing your order online.We have sent
                        you an email to <email className='font-weight-bold text-info'>{localStorage.getItem('user_email')}</email> with your order details.
                    </p>
                    <p className='col-sm-6 col-xs-offset-3'><Link to='/products' className="btn btn-success"> Go
                        Back </Link></p>
                    <br/><br/>
                </div>
            </div>
        )
    }
}

export default Success;
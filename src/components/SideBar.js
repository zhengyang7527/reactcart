import React, {Component} from 'react';
import '../css/ProductList.css';
import UserForm from "./UserForm";

class SideBar extends Component {

    componentWillMount() {
        window.simpleCart({
            cartColumns: [
                {
                    view: function (item, column) {
                        return "<img class='thumb' style='width: 60px;' src='" + item.get('thumb') + "'>";
                    },
                    attr: 'thumb',
                    label: false
                },
                {
                    view: function (item, column) {
                        return "<span class='text-dark'>" + item.get('name') + "</span>";
                    },
                    attr: 'name',
                    label: function (item, column) {
                        return "<label class='text-center'>Name</label>";
                    }
                },
                {
                    view: function (item, column) {
                        return "<input style='width: 40px;border: 1px; solid' type='text' value='" + item.get('quantity') + "' class='simpleCart_input'>";
                    },
                    attr: 'quantity',
                    label: function (item, column) {
                        return "<h4 class='text-center'>Qty</h4>";
                    }
                },
                {view: "remove", text: "Remove", label: false}
            ],
            cartStyle: "table"
        });
    }

    // componentDidMount(){
    //     window.simpleCart.update();
    // }

    render() {
        return (
            <div className="sidebar listBox">

                <h2>Your Cart</h2>

                <br/>
                <div className="simpleCart_items">
                </div>

                <div className="sc_demo_items">
                </div>

                <br/>
                <p>
                    Cart: <span className="simpleCart_total"></span> (<span id="simpleCart_quantity"
                                                                            className="simpleCart_quantity"></span>
                    items)
                </p>
                <br/>
                <br/>
                {this.props.user_display_name === null ? (
                    <div>

                    </div>
                ) : (
                    <h2>Hi, {this.props.user_display_name}</h2>
                )}

                <label>Order details to <span className='text-primary'>{this.props.user_email}</span></label>
                <UserForm handleChange={this.props.handleChange.bind(this)}/>
                <br/>
                <br/>
                <a href="javascript:;" className="btn btn-danger simpleCart_empty">empty cart</a>
                &nbsp;&nbsp;
                <a href="javascript:;" className="btn btn-success simpleCart_checkout">checkout</a>


            </div>
        )
    }
}

export default SideBar;
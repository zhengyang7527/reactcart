import React, {Component} from 'react';
import '../css/Header.css'
import logo from '../img/Logo_gold.png'

class Header extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    render() {
        return (
            <header className="App-header">
                {/*<img src={logo} className="App-logo" alt="logo"/>*/}
                <h1 className="App-title hidden-xs"><img src={logo} alt='Golden Harbor Trading Inc.' width='30%'/></h1>
                <h1 className="App-title visible-xs"><img src={logo} alt='Golden Harbor Trading Inc.' width='80%'/></h1>
            </header>
        )
    }
}

export default Header;

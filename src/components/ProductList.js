import React, {Component} from 'react';
import SearchInput, {createFilter} from 'react-search-input'
// import json_products from '../products.json'
import Product from './Product'
import '../css/ProductList.css'


// const KEYS_TO_FILTERS = ['name', 'price', 'category']

class ProductList extends Component {
    render() {
        return (
            <div className='listBox'>
                <div className="row">
                    <div>
                        {this.props.filteredProducts.map(product => {
                            return (
                                <div key={product.name} className="simpleCart_shelfItem col-xs-6 col-sm-4">
                                    <Product product_info={product}/>
                                </div>
                            )
                        })}
                    </div>
                </div>
            </div>
        );
    }
}

export default ProductList;

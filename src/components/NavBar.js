import React, {Component} from 'react';
import SearchInput from 'react-search-input'
import {Link} from 'react-router-dom'

import '../css/Header.css'


class NavBar extends Component {
    render() {
        return (
            <div className='row navBar'>
                <div className='col-xs-7 col-sm-4'>
                    <a href='javascript:;' id=' ' onClick={this.props.chooseCategory}>All 全部</a>
                    <span> | </span>
                    <a href='javascrip:;' id='frozen' onClick={this.props.chooseCategory}>Frozen 冻货</a>
                    <span> | </span>
                    <a href='javascript:;' id='Dry' onClick={this.props.chooseCategory}>Dry 干货</a>
                    <span> | </span>
                    <a href='javascript:;' id='fresh' onClick={this.props.chooseCategory}>Fresh 生鲜</a>
                </div>
                <div className='col-xs-3 col-sm-1'>
                    <Link to='/login' onClick={this.props.handleLogout}>Logout</Link>
                </div>
                <div className='col-xs-7 col-sm-2'>
                    <SearchInput className="search-input" onChange={this.props.searchUpdated}/>
                </div>
            </div>
        )
    }
}

export default NavBar;
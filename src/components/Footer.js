import React, {Component} from 'react';
import '../css/Header.css'

class Footer extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    render() {
        return (
            <footer className="footer navbar-fixed-bottom">
                <span>Copyright © 2017 Golden Harbor all Rights Reserved.</span>
                <span className='pull-right'>Powered by <a href="http://www.lokipro.ca" rel="noopener noreferrer"
                                   style={{color:'white'}} target='_blank'>Loki Pro Inc.</a></span>
            </footer>
        )
    }
}

export default Footer;

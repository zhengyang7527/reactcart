import React, {Component} from 'react'

export default class CompanyInfo extends Component {
    render() {
        return (
            <div className='row m-3' style={{paddingBottom: 20, marginBottom: 30}}>
                <div className='col-sm-3'></div>
                <div className='col-sm-4' style={{textAlign: 'left', paddingLeft: 30, paddingBottom: 30}}>
                    <span><b>Golden Harbor Trading Inc.</b> is a <b>FOOD SERVICE distribution company</b> that focusing on local Asian restaurants in St. John's Newfoundland. We supply Asian style dry food, frozen food and so much more. Providing the best value products and services to our customers is the company goal. </span><br/>
                </div>
                <div className='col-sm-3' style={{textAlign: 'left', paddingLeft: 30}}>
                    <span><b>Address 公司地址：</b>2 Dundee Avenue，Mount Pearl，NL，A1N 4R7</span><br/>
                    <span><b>Phone 电话：</b>（709)986-2129</span><br/>
                    <span><b>Email 邮件:</b> Lee@goldenharbor.ca</span><br/>
                    <span><b>Website 网址:</b>www.goldenharbor.ca</span><br/>
                </div>
                <div>
                    <br/>
                    <br/>
                </div>
            </div>
        )
    }
}
import React, {Component} from 'react'
import {Redirect, Route} from 'react-router'
import '../css/Login.css'
import axios from "axios/index";

class Login extends Component {


    render() {

        return (
            <div className="">
                <div className='row' style={{padding: 30}}>
                    <span>Please login in to see our product list. For new customers, you can call or email us to obtain the access information</span><br/>
                    <br/>
                    <span>感谢您选择金港贸易，请您使用提供的账号密码登录产品系统。新用户可通过电话或者邮件联系我们获得登录信息。</span><br/>
                </div>
                <div className="login-page form" style={{marginTop: 20}}>
                    <form className="login-form" onSubmit={this.props.handleSubmit}>
                        <input type="text" name='username' placeholder="username" onChange={this.props.handleChange}
                        />
                        <input type="password" name='password' placeholder="password" onChange={this.props.handleChange}
                        />
                        <button type='submit'>login</button>
                    </form>
                </div>
            </div>
        );
    }


}

export default Login;